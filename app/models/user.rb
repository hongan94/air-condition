class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :email, presence: true, length: { in: 5..100 }, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i }
  validates :name, presence: true
  validates :phone, uniqueness: true,
                    numericality: true,
                    length: { minimum: 10, maximum: 15 },
                    allow_blank: true
  before_save :translate_slug
  has_one_attached :avatar
  default_scope { order(created_at: :desc) }

  def password_required?
    new_record? ? super : false
  end

  def translate_slug
    self.slug = format_string_with_unsigned(self.name)
  end
end
