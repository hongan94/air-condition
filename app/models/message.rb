class Message < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  before_save :translate_slug
  scope :message_not_seen, -> { where(seen: false) }
  default_scope { order(created_at: :desc) }
  def translate_slug
    self.slug = format_string_with_unsigned(self.name)
  end
end
