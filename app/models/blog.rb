class Blog < ApplicationRecord
  has_one_attached :image
  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, presence: true
  before_save :translate_slug
  enum kind: ['Máy lạnh', 'Tủ lạnh', 'Máy giặt', 'Loại khác']
  default_scope { order(created_at: :desc) }

  def translate_slug
    self.slug = format_string_with_unsigned(self.title)
  end
end
