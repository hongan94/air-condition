class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def normalize_characters(str)
    str = str.gsub(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/, "a");
    str = str.gsub(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/, "e");
    str = str.gsub(/ì|í|ị|ỉ|ĩ/, "i");
    str = str.gsub(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/, "o");
    str = str.gsub(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/, "u");
    str = str.gsub(/ỳ|ý|ỵ|ỷ|ỹ/, "y");
    str = str.gsub(/đ/, "d");

    str = str.gsub(/\u0300|\u0301|\u0303|\u0309|\u0323/, "")
    str = str.gsub(/\u02C6|\u0306|\u031B/, "")
  end

  def format_string_with_unsigned(str)
    string_formated = if I18n.locale == :vi
      normalize_characters(str).gsub(/　| |,|_|\./, '-')
    else
      str.gsub(/　| |,|_|\./, '-')
    end
    string_formated
  end
end
