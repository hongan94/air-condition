class Service < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :name, presence: true
  has_one_attached :image
  default_scope { order(created_at: :desc) }
  before_save :translate_slug

  def translate_slug
    self.slug = format_string_with_unsigned(self.name)
  end
end
