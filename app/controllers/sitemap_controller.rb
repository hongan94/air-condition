class SitemapController < ApplicationController

  def index
    respond_to do |format|
      format.xml
    end
  end

  def robots
    if canonical_host?
      render 'allow'
    else
      render 'disallow'
    end
  end

  private

  def canonical_host?
    request.host =~ /plugingeek\.com/
  end

end