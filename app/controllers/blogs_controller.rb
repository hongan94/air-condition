class BlogsController < ApplicationController
  layout 'user_layout'

  def index
    @blogs = Blog.all.page(params[:page]).per(9)
    prepare_meta_tags title: 'Bài viết'
  end

  def show
    @blog = Blog.friendly.find(params[:id])
    if @blog.keywords.present?
      prepare_meta_tags title: @blog.title, description: @blog.description, keywords: @blog.keywords
    else
      prepare_meta_tags title: @blog.title, description: @blog.description, keywords: 'dien lanh long an,điện lạnh tân an, điện lạnh long an, sửa máy lạnh long an, sửa máy giặt tại long an,sửa tủ lạnh tại long an'
    end
  end
end