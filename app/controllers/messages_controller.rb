class MessagesController < ApplicationController

  def create
    content = Rack::Utils.parse_nested_query(params[:content])
    message = Message.new(content)
    respond_to do |format|
      if message.save
        format.json { render json: message,  status: :ok }
      else
        format.json { render json: message.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def message_params
    params.require(:message).permit(:name, :email, :address, :content, :phone)
  end
end