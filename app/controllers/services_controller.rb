class ServicesController < ApplicationController
  layout 'user_layout'

  def index
    @services  = Service.all.page(params[:page]).per(9)
    prepare_meta_tags title: "Dịch vụ"
  end

  def show
    @service = Service.friendly.find(params[:id])
    if @service.keywords.present?
      prepare_meta_tags title: @service.name, description: @service.description, keywords: @service.keywords
    else
      prepare_meta_tags title: @service.name, description: @service.description, keywords: 'dien lanh long an,điện lạnh tân an, điện lạnh long an, sửa máy lạnh long an, sửa máy giặt tại long an,sửa tủ lạnh tại long an'
    end
  end
end