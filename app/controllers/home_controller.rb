class HomeController < ApplicationController
  layout 'user_layout'

  def index
    prepare_meta_tags title: "Trang chủ", description: "Điện lạnh Nguyễn Trắng, Điện lạnh Long An chuyên bảo trì, sửa chữa, mua bán, lắp đặt máy lạnh, tủ lạnh, máy giặt, máy tắm nóng lạnh tại Long An uy tín chất lượng giá rẻ", keywords: "dien lanh long an,điện lạnh tân an, điện lạnh long an, sửa máy lạnh long an, sửa máy giặt tại long an,sửa tủ lạnh tại long an"
  end

  def introduces
    @service = Service.all
    prepare_meta_tags title: "Giới thiệu", description: "Sửa chữa điện lạnh Nguyễn Trắng chất lượng hàng đầu Long An - Tân An - Thủ Thừa với hơn 10 năm kinh nghiệm sửa chữa máy lạnh , tủ lạnh , máy giặt, máy tắm nóng lạnh, tay nghề cao, phục vụ tận tình, chuyên nghiệp", keywords: 'điện lạnh long an, dien lanh long an, sua may lanh long an, sua may giạt long an, sua tu lanh long an'
  end

  def contact
    prepare_meta_tags title: "Liên hệ", description: "Điện lạnh Long An Nguyễn Trắng chuyên sửa chữa về điện lạnh như sửa chữa máy lạnh , tủ lạnh , máy giặt, máy tắm nóng lạnh ở long an, thử thừa, tân an, chất lượng hàng đầu , liên hệ số điện thoại 0793750267 ", keywords: "điện lạnh long an, dien lanh long an, sua may lanh long an, sua may giạt long an, sua tu lanh long an"
  end
end
