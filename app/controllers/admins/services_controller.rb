module Admins
  class ServicesController < ApplicationController
    before_action :find_service, only: [:edit, :update, :destroy, :show]
    def index
      @services  = Service.all.page(params[:page]).per(10)
    end

    def new
      @service = Service.new
    end

    def create
      @service = Service.new(service_params)
      if @service.save
        redirect_to admins_services_path, notice: 'Đã tạo thành công'
      else
        render :new
      end
    end

    def edit
    end

    def show
    end

    def update
      if @service.update(service_params)
        redirect_to admins_services_path, notice: 'Cập nhật thành công'
      else
        render :edit, notice: 'Cập nhật thất bại'
      end
    end

    def destroy
      if @service.destroy
        redirect_to admins_services_path, notice: 'Xóa thành công'
      else
        redirect_to admins_services_path, notice: 'Xóa thất bại'
      end
    end

    private
    def service_params
      params.require(:service).permit(:name, :description, :content, :image, :keywords)
    end

    def find_service
      @service = Service.friendly.find(params[:id])
    end
  end
end