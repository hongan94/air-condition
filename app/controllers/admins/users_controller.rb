module Admins
  class UsersController < ApplicationController
    before_action :find_user, only: [:edit, :update, :destroy, :show]
    def index
      @users  = User.all.page(params[:page]).per(10)
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)
      if @user.save
        redirect_to admins_users_path, notice: 'Đã tạo thành công'
      else
        render :new
      end
    end

    def edit
    end

    def show
    end

    def update
      if @user.update(user_params)
        redirect_to admins_users_path, notice: 'Cập nhật thành công'
      else
        render :edit, notice: 'Cập nhật thất bại'
      end
    end

    def destroy
      if @user.destroy
        redirect_to admins_users_path, notice: 'Xóa thành công'
      else
        redirect_to admins_users_path, notice: 'Xóa thất bại'
      end
    end

    private
    def user_params
      params.require(:user).permit(:name, :email, :password, :phone, :gender, :avatar)
    end

    def find_user
      @user = User.friendly.find(params[:id])
    end
  end
end