module Admins
  class BlogsController < ApplicationController
    before_action :find_blog, only: [:edit, :update, :destroy, :show]
    def index
      @blogs  = Blog.all.page(params[:page]).per(9)
    end

    def new
      @blog = Blog.new
    end

    def create
      @blog = Blog.new(blog_params)
      if @blog.save
        redirect_to admins_blogs_path, notice: 'Đã tạo thành công'
      else
        render :new
      end
    end

    def edit
    end

    def show
    end

    def update
      if @blog.update(blog_params)
        redirect_to admins_blogs_path, notice: 'Cập nhật thành công'
      else
        render :edit, notice: 'Cập nhật thất bại'
      end
    end

    def destroy
      if @blog.destroy
        redirect_to admins_blogs_path, notice: 'Xóa thành công'
      else
        redirect_to admins_blogs_path, notice: 'Xóa thất bại'
      end
    end

    private
    def blog_params
      params.require(:blog).permit(:title, :description, :content, :kind, :image, :keywords)
    end

    def find_blog
      @blog = Blog.friendly.find(params[:id])
    end
  end
end