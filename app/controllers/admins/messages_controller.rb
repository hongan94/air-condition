module Admins
  class MessagesController < ApplicationController

    def index
      @messages = Message.all.page(params[:page]).per(10)
    end

    def show
      @message = Message.friendly.find(params[:id])
      @messages.update(seen: true)
    end
  end
end