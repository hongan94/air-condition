module Admins
  class ApplicationController < ActionController::Base
    before_action :authenticate_user!
    before_action :get_message

    add_flash_types :info, :error, :warning

    layout 'application'
    protect_from_forgery with: :exception

    def after_sign_in_path_for(resource)
      admin_root_path
    end

    def after_sign_out_path_for(resource_or_scope)
      new_user_session_path
    end

    def get_message
      @messages = Message.message_not_seen
      @blogs = Blog.all
      @users = User.all
      @services = Service.all
    end
  end
end