class ApplicationController < ActionController::Base
  before_action :prepare_meta_tags,if: -> { request.get? }
  before_action :setting_object

  def prepare_meta_tags(options={})
    site_name   = "Điện lạnh long an | Điện lạnh Nguyễn Trắng | dien lanh long an"
    title       = options[:title] || [controller_name, action_name].join(" ")
    description = options[:description] || "Điện lạnh Nguyễn Trắng, Điện lạnh Long An chuyên bảo trì, sửa chữa, mua bán, lắp đặt máy lạnh, tủ lạnh, máy giặt, nhận làm đồng tủ lạnh, máy giặt"
    keywords = options[:keywords] || "dien lanh long an,điện lạnh tân an, điện lạnh long an, sửa máy lạnh long an, sửa máy giặt tại long an,sửa tủ lạnh tại long an"
    image       = options[:image] || "http://dienlanhlongan.info/not_found"
    current_url = request.url

    # Let's prepare a nice set of defaults
    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords: keywords,
      twitter: {
        site_name: site_name,
        site: 'dienlanhlongan.info',
        card: 'summary',
        description: description,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end

  def setting_object
    @services = Service.order(created_at: :asc).limit(10)
    @blogs = Blog.order(created_at: :desc).limit(5)
    @blogs_latest = Blog.order(created_at: :desc).limit(5)
  end
end
