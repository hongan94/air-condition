module ApplicationHelper

  def format_date(value)
    value.utc.strftime("%d-%m-%Y %H:%M:%S")
  end

  def type_blogs
    [
      ['Máy lạnh', 'Máy lạnh'],
      ['Tủ lạnh', 'Tủ lạnh'],
      ['Máy giặt', 'Máy giặt'],
      ['Loại khác', 'Loại khác']
    ]
  end
end
