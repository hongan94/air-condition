
threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }
threads threads_count, threads_count

# Specifies the `environment` that Puma will run in.
# Defaults to development

app_dir = File.expand_path("../..", __FILE__)
directory app_dir
shared_dir = "#{app_dir}/tmp"

rails_env = 'production'

if %w[production staging].member?(rails_env)
  # Logging
  stdout_redirect "#{app_dir}/log/puma.stdout.log", "#{app_dir}/log/puma.stderr.log", true

  # Set master PID and state locations
  pidfile "#{shared_dir}/pids/puma.pid"
  state_path "#{shared_dir}/pids/puma.state"

  # Change to match your CPU core count
  workers 1

  preload_app!

  # Set up socket location
  bind "unix://#{shared_dir}/sockets/puma.sock"

  before_fork do
    # app does not use database, uncomment when needed
    # ActiveRecord::Base.connection_pool.disconnect!
  end

  on_worker_boot do
    ActiveSupport.on_load(:active_record) do
      # app does not use database, uncomment when needed
      # db_url = ENV.fetch('DATABASE_URL')
      # puts "puma: connecting to DB at #{db_url}"
      # ActiveRecord::Base.establish_connection(db_url)
    end
  end
end
plugin :tmp_restart
