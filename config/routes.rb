Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  devise_for :users, path: 'admins', controllers: { sessions: 'admins/sessions', registrations: 'admins/registrations', passwords: 'admins/passwords'}
  namespace :admins, path: 'quan-ly' do
    root to: "home#index"
    resources :services, path: 'dich-vu'
    resources :users, path: 'nguoi-dung'
    resources :messages, path: 'tin-nhan'
    resources :blogs, path: 'bai-viet'
  end

  root to: "home#index"
  get 'gioi-thieu', to: "home#introduces"
  get 'lien-he', to: "home#contact"
  resources :services, path: 'dich-vu'
  resources :messages, path: 'tin-nhan'
  resources :blogs, path: 'bai-viet'
  get "/sitemap.xml" => "sitemap#index", :format => "xml", :as => :sitemap
  get 'robots.:format' => 'sitemap#robots'

  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
end
