class AddContentToService < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :content, :text
  end
end
