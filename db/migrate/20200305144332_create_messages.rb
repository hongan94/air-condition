class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.string :name
      t.string :email
      t.string :address
      t.string :content
      t.string :phone
      t.boolean :seen, default: false
      t.timestamps
    end
  end
end
