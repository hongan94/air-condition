class AddKeywordToService < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :keywords, :string
  end
end
