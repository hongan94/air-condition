class AddKeywordToBlogs < ActiveRecord::Migration[6.0]
  def change
    add_column :blogs, :keywords, :string
  end
end
