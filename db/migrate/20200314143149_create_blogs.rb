class CreateBlogs < ActiveRecord::Migration[6.0]
  def change
    create_table :blogs do |t|
      t.string :title, null: false
      t.string :description
      t.text :content
      t.integer :kind
      t.string :slug
      t.timestamps
      t.index [ :slug ], unique: true
    end
  end
end
